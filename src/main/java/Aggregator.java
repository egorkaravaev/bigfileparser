import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public enum Aggregator {

    INSTANCE;

    final Map<String, List<String>> resultMap = new ConcurrentHashMap<>();

    public void putMatch(String name, String value) {
         synchronized (resultMap) {
             List<String> values = resultMap.get(name);
             if (values == null) {
                 List<String> offsets = new ArrayList<>();
                 offsets.add(value);
                 resultMap.put(name, offsets);
             } else {
                 values.add(value);
                 resultMap.put(name, values);
             }
         }
    }

    public void printResults() {
        resultMap.forEach((k,v) -> {
            StringBuilder finalValue = new StringBuilder();
            for (String value : v) {
                finalValue.append(value).append(" ");
            }
            System.out.println(k + " --> " + finalValue.toString());
        });
    }
}
