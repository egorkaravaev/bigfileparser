import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Names {

    public static final List<String> names = Arrays.asList(
            "James", "John", "Robert", "Michael", "William", "David", "Richard", "Charles", "Joseph", "Thomas", "Christopher",
            "Daniel", "Paul", "Mark", "Donald", "George", "Kenneth", "Steven", "Edward", "Brian", "Ronald", "Anthony", "Kevin",
            "Jason", "Matthew", "Gary", "Timothy", "Jose", "Larry", "Jeffrey", "Frank", "Scott", "Eric", "Stephen", "Andrew",
            "Raymond", "Gregory", "Joshua", "Jerry", "Dennis", "Walter", "Patrick", "Peter", "Harold", "Douglas", "Henry",
            "Carl", "Arthur", "Ryan", "Roger");

    public static final Map<String, Pattern> PATTERNS = new HashMap<>();

    static {
        for (String name : names) {
            Pattern pattern = Pattern.compile(name);
            PATTERNS.put(name, pattern);
        }
    }
}
