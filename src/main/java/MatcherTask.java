import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatcherTask implements Runnable {

    private String line;
    private int i;
    private int chunkCount;

    public MatcherTask(String line, int i, int chunkCount) {
        this.line = line;
        this.i = i;
        this.chunkCount = chunkCount;
    }

    @Override
    public void run() {
        Names.PATTERNS.forEach((name,pattern) -> {
            Matcher matcher = pattern.matcher(line);
            while (matcher.find()) {
                StringBuilder builder = new StringBuilder();
                int lineOffset = (chunkCount - 1) * 1000 + i + 1;
                builder.append("[lineOffset=").append(lineOffset).append(", charOffset=").append(matcher.start()).append("]");
                Aggregator.INSTANCE.putMatch(name, builder.toString());
            }
        });
    }
}
