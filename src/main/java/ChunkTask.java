import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ChunkTask implements Runnable {

    private List<String> lines;
    private int chunkCount;

    public ChunkTask(List<String> nextLines, int chunkCount) {
        this.lines = nextLines;
        this.chunkCount = chunkCount;
    }

    @Override
    public void run() {
        for (int i = 0; i < lines.size(); i++) {
            String line = lines.get(i);
            if (line.length() > 3) {
                MatcherTask matcherTask = new MatcherTask(line, i, chunkCount);
                ThreadsExecutor.INSTANCE.execute(matcherTask);
            }
        }
    }
}
