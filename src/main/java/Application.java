import com.google.common.collect.Iterators;
import com.google.common.util.concurrent.ListenableFuture;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;

public class Application {

    private static final File file = new File("big.txt");

    public static void main(String[] args) {
        long start = System.currentTimeMillis();

        try (Stream<String> stream = Files.lines(file.toPath())) {
            Iterator<List<String>> iterator = Iterators.partition(stream.iterator(), 10000);
            int chunkCount = 1;
            while (iterator.hasNext()) {
                ChunkTask chunkTask = new ChunkTask(iterator.next(), chunkCount);
                ThreadsExecutor.INSTANCE.execute(chunkTask);
                chunkCount++;
            }
            for (ListenableFuture<?> future : ThreadsExecutor.INSTANCE.getFutures()) {
                future.get();
            }
            Aggregator.INSTANCE.printResults();

        } catch (IOException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println("\nit takes: " + (end - start) + " ms");
    }


}
