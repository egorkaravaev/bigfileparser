import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import java.util.Queue;
import java.util.concurrent.*;

public enum ThreadsExecutor {

    INSTANCE;

    public ListeningExecutorService service = MoreExecutors.listeningDecorator(
            customPool(Runtime.getRuntime().availableProcessors(), 10000));

    private static ExecutorService customPool(int nThreads, int queueSize) {
        return new ThreadPoolExecutor(nThreads, nThreads, 5000L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(queueSize), new ThreadPoolExecutor.CallerRunsPolicy());
    }

    private Queue<ListenableFuture<?>> futures = new ConcurrentLinkedQueue<>();

    public void execute(Runnable task) {
        ListenableFuture<?> listenableFuture = service.submit(task);
        futures.add(listenableFuture);
    }

    public Queue<ListenableFuture<?>> getFutures() {
        return futures;
    }
}
