import com.google.common.collect.Iterators;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class FileReadingTest {

    private File file = new File("big.txt");

    @Test
    public void test() {
        try {
            Thread.sleep(30000L);
            System.out.println("starting...");
            testGuavaIterators();
            Thread.sleep(30000L);
            System.out.println("starting...");
            testFileUtils();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void testFileUtils() {
        System.out.println("Testing memory 1");
        try {
            long start = System.currentTimeMillis();
            List<String> lines = FileUtils.readLines(file);
            long end = System.currentTimeMillis();
            System.out.println("it takes1: " + (end - start) + " ms " + lines.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void testGuavaIterators() {
        System.out.println("Testing memory 2");
        List<String> commonList = new ArrayList<>();
        long start = System.currentTimeMillis();
        try (Stream<String> stream = Files.lines(file.toPath())) {
            Iterator<List<String>> iterator = Iterators.partition(stream.iterator(), 1000);
            while (iterator.hasNext()) {
                commonList.addAll(iterator.next());
                Thread.sleep(500L);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println("it takes2: " + (end - start) + " ms " + commonList.size());
    }
}
